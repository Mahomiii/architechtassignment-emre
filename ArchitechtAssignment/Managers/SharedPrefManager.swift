//
//  SharedPrefs.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import Foundation


class SharedPrefManager {
    static let shared = SharedPrefManager()
    private init() {}
    
    private let storage = UserDefaults.standard
    
    func getValueWithKey(_ key: String) -> String
    {
        let val = storage.string(forKey: key)
        return val ?? ""
    }
    
    func setValueWithKey(_ key: String, _ value: String)
    {
        storage.set(value, forKey: key)
        storage.synchronize()
    }
}
