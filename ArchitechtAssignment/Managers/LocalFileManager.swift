//
//  FileManager.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 19.06.2022.
//

import UIKit

enum SaveError: Error {
    case notFound
    case unableToSave
    case alreadySaved
}

class LocalFileManager {
    static let shared = LocalFileManager()
    
    let documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

    func saveImageToFile(image: UIImage, fileName: String, _ completion: @escaping (Result<String, SaveError>) -> ()) {
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            if let imageData = image.pngData() {
                if !FileManager.default.fileExists(atPath: fileURL.path) {
                    try imageData.write(to: fileURL, options: .atomic)
                    completion(.success("ImageSaved".localized()))
                }
                else {
                    completion(.failure(.alreadySaved))
                }
            }
            else {
                completion(.failure(.notFound))
            }
            
        } catch {
            completion(.failure(.unableToSave))
        }
    }
    
    func getImages(_ completion: @escaping ([String]) -> ()) {
        var data: [String] = []
        do {
            var fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
            fileURLs.sortByCreateDate()
                
            fileURLs.forEach { it in
                data.append(it.lastPathComponent)
            }
            completion(data)
        } catch {
            completion(data)
        }
    }
    
}
