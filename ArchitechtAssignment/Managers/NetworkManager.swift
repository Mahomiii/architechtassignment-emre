//
//  NetworkManager.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import Foundation

enum NetworkError: Error {
    case genericError
}

class NetworkManager {
    
    static var shared = NetworkManager()
    
    func request<T: Decodable>(_ type: T.Type, _ url: URL,_ parameters: [String : Any]? = nil, _ completion: @escaping (Result<T, NetworkError>) -> ()) {
        let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
            if error == nil {
                guard let data = data else { return }
                do {
                    let response = try JSONDecoder().decode(T.self, from: data)
                    DispatchQueue.main.async {
                        completion(.success(response))
                    }
                } catch {
                    completion(.failure(.genericError))
                }
                
            }
            else {
                completion(.failure(.genericError))
            }
        }
        task.resume()
    }
}
