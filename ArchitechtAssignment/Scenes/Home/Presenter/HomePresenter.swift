//
//  HomePresenter.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

protocol HomePresenterProtocol {
    var view: HomeViewProtocol? { get set }
    var interactor: HomeInteractorProtocol { get }
    var router: HomeRouterProtocol { get }
    
    func viewDidLoad()
    func viewWillAppear()
    
    func getRandomImage()
    func saveImage(imageView: MainImageView)
    func goSavedImages(from: UIViewController)
}
    
final class HomePresenter: HomePresenterProtocol {

    weak var view: HomeViewProtocol?
    let interactor: HomeInteractorProtocol
    let router: HomeRouterProtocol
    var currentUrl: String?

    init(interactor: HomeInteractorProtocol,
         router: HomeRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        interactor.getData()
    }

    func viewWillAppear() {
    }
}

extension HomePresenter: HomeInteractorOutputProtocol {
    func didSucceed(model: DogResponse?) {
        view?.configure(model: model)
        currentUrl = model?.message
    }
    
    func didFail(reason: String) {
        router.showAlert(title: "SomethingWrong".localized(), detail: "", okButtonHandler: nil)
    }
    
    func didSaveImage(message: String) {
        router.showAlert(title: "ImageSaved".localized(), detail: "", okButtonHandler: nil)
    }
    
    func didFailImage(error: SaveError) {
        var errorMessage: String?
        switch error {
        case .notFound:
            errorMessage = "NotFound"
        case .unableToSave:
            errorMessage = "UnableToSave"
        case .alreadySaved:
            errorMessage = "AlreadySaved"
        }
        router.showAlert(title: errorMessage?.localized() ?? "", detail: "", okButtonHandler: nil)
    }
    
    func getRandomImage() {
        interactor.getData()
    }
    
    func saveImage(imageView: MainImageView) {
        interactor.saveImage(imageView: imageView)
    }
    
    func goSavedImages(from: UIViewController) {
        router.goSavedImages(from: from)
    }
    
    

}
