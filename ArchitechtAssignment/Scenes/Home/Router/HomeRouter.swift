//
//  HomeRouter.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

protocol HomeRouterProtocol: BaseRouterProtocol {
    func goSavedImages(from: UIViewController)
}

final class HomeRouter: HomeRouterProtocol {
    
    let from: UIViewController
    
    init(from: UIViewController ) {
        self.from = from
    }
    
    func goSavedImages(from: UIViewController) {
        let vc = SavedImagesBuilder.generate()
        if let vc = vc {
            Navigator.shared.push(vc, from: from)
        }
    }
    
    func showAlert(title: String, detail: String, okButtonHandler: (() -> ())? = nil) {
         from.showAlert(title: title, message: detail, okButtonHandler: okButtonHandler)
     }
}

