//
//  HomeViewController.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

protocol HomeViewProtocol: AnyObject {
    var presenter: HomePresenterProtocol! { get }
    func configure(model: DogResponse?)
}

final class HomeViewController: UIViewController {
    
    @IBOutlet private weak var imgDog: MainImageView!
    @IBOutlet private weak var btnGetRandom: MainButton!
    @IBOutlet private weak var btnSave: MainButton!
    @IBOutlet private weak var btnSavedImages: MainButton!
    
    var presenter: HomePresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Dogs!"
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear( _ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    @IBAction func getRandomImage( _ sender: UIButton) {
        presenter.getRandomImage()
    }
    
    @IBAction func saveImage( _ sender: UIButton) {
        presenter.saveImage(imageView: imgDog)
    }
    
    @IBAction func goSavedImages( _ sender: UIButton) {
        presenter.goSavedImages(from: self)
    }
    
}

extension HomeViewController: HomeViewProtocol {
    
    func configure(model: DogResponse?) {
        if let message = model?.message {
            imgDog.imageFromURL(urlString: message)
            if let fileName = URL(string: message)?.lastPathComponent {
                imgDog.fileName = fileName
            }
        }
    }
    
}

