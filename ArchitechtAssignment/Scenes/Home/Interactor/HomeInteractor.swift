//
//  HomeInteractor.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import Foundation
import UIKit

protocol HomeInteractorProtocol {
    var output: HomeInteractorOutputProtocol? { get set }
    
    func getData()
    func saveImage(imageView: MainImageView)
}

protocol HomeInteractorOutputProtocol: AnyObject {
    func didSucceed(model: DogResponse?)
    func didFail(reason: String)
    
    func didSaveImage(message: String)
    func didFailImage(error: SaveError)
}

final class HomeInteractor: HomeInteractorProtocol {
    
    weak var output: HomeInteractorOutputProtocol?
    
    func getData() {
        guard let url = URL(string: randomImageUrl) else { return }
        NetworkManager.shared.request(DogResponse.self, url) { [weak self] result in
            switch result {
            case .success(let response): self?.output?.didSucceed(model: response)
            case .failure(let error): self?.output?.didFail(reason: error.localizedDescription)
                
            }
            
        }
    }
    
    func saveImage(imageView: MainImageView) {
        if let image = imageView.image {
            let sharedPrefs = SharedPrefManager.shared
            do {
                var imagesString = sharedPrefs.getValueWithKey(imagesKey)
                if imagesString.isEmpty { //check if userdefaults is empty (user's first time saving an image)
                    sharedPrefs.setValueWithKey(imagesKey, "[]") //if it's empty, save an empty array as a string
                    imagesString = sharedPrefs.getValueWithKey(imagesKey) //our string to be decoded now represents an empty array
                }
                
                var savedImages = try JSONDecoder().decode([SavedImage].self, from: imagesString.data(using: .utf8)!)
                //check if this image has been saved before
                if !savedImages.contains(where: {$0.fileName == imageView.fileName}) {
                    let savedImage = SavedImage(fileName: imageView.fileName, timestamp: Int64(Date().timeIntervalSince1970))
                    //save with timestamp in order to sort images
                    savedImages.append(savedImage)
                }
                else {
                    self.output?.didFailImage(error: .alreadySaved)
                    return
                }
                
                try sharedPrefs.setValueWithKey(imagesKey, String(data: JSONEncoder().encode(savedImages), encoding: .utf8)!)
                LocalFileManager.shared.saveImageToFile(image: image, fileName: imageView.fileName) { [weak self] result in
                    switch result {
                    case .success(let messsage): self?.output?.didSaveImage(message: messsage)
                    case .failure(let error): self?.output?.didFailImage(error: error)
                    }
                }
            } catch {
                self.output?.didFailImage(error: .unableToSave)
                return
            }
            
            
        }
    }
    
    
    
}
