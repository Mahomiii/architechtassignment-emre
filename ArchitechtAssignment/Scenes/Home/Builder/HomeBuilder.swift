//
//  HomeBuilder.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

enum HomeBuilder {
    static func generate() -> UIViewController? {
        let view = HomeViewController()
        var interactor: HomeInteractorProtocol = HomeInteractor()
        let router: HomeRouterProtocol = HomeRouter(from: view)
        var presenter: HomePresenterProtocol & HomeInteractorOutputProtocol = HomePresenter(interactor: interactor, router: router)
        view.presenter = presenter
        presenter.view = view
        interactor.output = presenter

        return view
    }
}
