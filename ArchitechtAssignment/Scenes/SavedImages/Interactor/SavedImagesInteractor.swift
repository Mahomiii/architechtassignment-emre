//
//  SavedImagesInteractor.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

protocol SavedImagesInteractorProtocol {
    var output: SavedImagesInteractorOutputProtocol? { get set }
    
    func getData()
}

protocol SavedImagesInteractorOutputProtocol: AnyObject {
    func didGetData(items: [SavedImage])
    func didFail()
}

final class SavedImagesInteractor: SavedImagesInteractorProtocol {
    
    weak var output: SavedImagesInteractorOutputProtocol?
    
    func getData() {
        do {
            let imagesString = SharedPrefManager.shared.getValueWithKey(imagesKey)
            //check if user has saved any images before
            if !imagesString.isEmpty {
                var images = try JSONDecoder().decode([SavedImage].self, from: imagesString.data(using: .utf8) ?? Data())
                images.sort(by: {$0.timestamp > $1.timestamp})
                self.output?.didGetData(items: images)
            }
            else {
                //if user hasn't saved any images send an empty array
                self.output?.didGetData(items: [])
            }
        }
        catch {
            self.output?.didFail()
        }
        
    }
}
