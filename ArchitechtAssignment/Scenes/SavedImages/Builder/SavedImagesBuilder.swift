//
//  SavedImagesBuilder.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

enum SavedImagesBuilder {
    static func generate() -> UIViewController? {

        let view = SavedImagesViewController()
        var interactor: SavedImagesInteractorProtocol = SavedImagesInteractor()
        let router: SavedImagesRouterProtocol = SavedImagesRouter(from: view)
        var presenter: SavedImagesPresenterProtocol & SavedImagesInteractorOutputProtocol = SavedImagesPresenter(interactor: interactor, router: router)
        view.presenter = presenter
        presenter.view = view
        interactor.output = presenter

        return view
    }
}
