//
//  SavedImagesPresenter.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

protocol SavedImagesPresenterProtocol {
    var view: SavedImagesViewProtocol? { get set }
    var interactor: SavedImagesInteractorProtocol { get }
    var router: SavedImagesRouterProtocol { get }
    
    func viewDidLoad()
    func viewWillAppear()
}
    
final class SavedImagesPresenter: SavedImagesPresenterProtocol {

    weak var view: SavedImagesViewProtocol?
    let interactor: SavedImagesInteractorProtocol
    let router: SavedImagesRouterProtocol

    init(interactor: SavedImagesInteractorProtocol,
         router: SavedImagesRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    func viewDidLoad() {
        interactor.getData()
    }

    func viewWillAppear() {
    }
}

extension SavedImagesPresenter: SavedImagesInteractorOutputProtocol {
    func didGetData(items: [SavedImage]) {
        view?.configureTableView(items: items)
    }
    
    func didFail() {
        router.showAlert(title: "SomethingWrong".localized(), detail: "", okButtonHandler: nil)
    }
}

extension SavedImagesPresenter: ImagesTableViewDelegateOutput {
    
}
