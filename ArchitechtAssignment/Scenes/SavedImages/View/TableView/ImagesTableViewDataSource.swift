//
//  ImagesTableViewDataSource.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

class ImagesTableViewDataSource: NSObject {
     
    private var items: [SavedImage]
    
    init(items: [SavedImage]) {
        self.items = items
    }
    
}

extension ImagesTableViewDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ImageTableViewCell.dequeue(from: tableView)
        let item = items[indexPath.row]
        cell.configure(fileName: item.fileName)
        cell.selectionStyle = .none
        return cell
    }
}

