//
//  ImageTableViewCell.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var imgDog: MainImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(fileName: String) {
        imgDog.loadImageFromFile(fileName: fileName)
    }
    
}
