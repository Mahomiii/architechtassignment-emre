//
//  SavedImagesViewController.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

protocol SavedImagesViewProtocol: AnyObject {
    var presenter: SavedImagesPresenterProtocol! { get }
    func configureTableView(items: [SavedImage])
}

final class SavedImagesViewController: UIViewController {
    
    @IBOutlet private weak var mTableView: UITableView!
    @IBOutlet private weak var lblNoData: UILabel!
    
    private var tableViewDelegate: ImagesTableViewDelegate?
    private var tableViewDataSource: ImagesTableViewDataSource?
    
    var presenter: SavedImagesPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "SavedImages".localized()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear( _ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    
}

extension SavedImagesViewController: SavedImagesViewProtocol {
    func configureTableView(items: [SavedImage]) {
        DispatchQueue.main.async { [weak self] in
            if !items.isEmpty { //configure table view if user have saved any images
                self?.tableViewDelegate = ImagesTableViewDelegate()
                self?.tableViewDataSource = ImagesTableViewDataSource(items: items)
                self?.mTableView.separatorStyle = .none
                self?.mTableView.separatorInset = .zero
                self?.mTableView.delegate = self?.tableViewDelegate
                self?.mTableView.dataSource = self?.tableViewDataSource
                self?.mTableView.reloadData()
            }
            else { //if user hasn't saved any images instead of preparing table view, show a label stating no data found
                self?.lblNoData.text = "NoData".localized()
                self?.lblNoData.isHidden = false
            }
        }
    }
    
}

