//
//  SavedImagesRouter.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

protocol SavedImagesRouterProtocol: BaseRouterProtocol {

}

final class SavedImagesRouter: SavedImagesRouterProtocol {
    let from: UIViewController
    
    init(from: UIViewController ) {
        self.from = from
    }
    
    func showAlert(title: String, detail: String, okButtonHandler: (() -> ())?) {
        from.showAlert(title: title, message: detail, buttonTitle: "Ok".localized(), okButtonHandler: okButtonHandler)
    }
    
    
}
