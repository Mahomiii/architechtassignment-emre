//
//  BaseRouterProtocol.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

protocol BaseRouterProtocol {
    func showAlert(title: String, detail: String, okButtonHandler: (() -> ())?)
}
