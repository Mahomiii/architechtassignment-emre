//
//  MainImageView.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

class MainImageView: BaseImageView {
    
    var fileName = ""
    
    override func configure() {
        contentMode = .scaleAspectFit
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = defaultCornerRadius
        clipsToBounds = true
    }
    
}
