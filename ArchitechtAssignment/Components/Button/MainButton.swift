//
//  MainButton.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

class MainButton: BaseButton {

    override func configure() {
        setTitleColor(.white, for: [])
        backgroundColor = .purple
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = defaultCornerRadius
    }
    
}
