//
//  BaseImageView.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

open class BaseImageView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        configure()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }

    open override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        configure()
    }

    internal func configure() {
        preconditionFailure("configure - This method must be overridden")
    }
}
