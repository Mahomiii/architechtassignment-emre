//
//  UITableViewCell+Extensions.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import UIKit

protocol UITableViewCellDequeueable {
    
}

extension UITableViewCell: UITableViewCellDequeueable {

    @objc class var msReuseIdentifier: String {
        return String(describing: self)
    }

    @objc class func nib() -> UINib! {
        let className = String(describing: self)

        return UINib(nibName: className, bundle: nil)
    }

}

extension UITableViewCellDequeueable where Self: UITableViewCell {
    
    static func dequeue(from tableView: UITableView, reuseIdentifier: String! = nil) -> Self {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier
        
        guard let cell = self.optDequeue(from: tableView, reuseIdentifier: reuseIdentifier) else {
            register(to: tableView, reuseIdentifier: reuseIdentifier)
            return optDequeue(from: tableView, reuseIdentifier: reuseIdentifier)
        }
        
        return cell
    }
    
    static func optDequeue(from tableView: UITableView, reuseIdentifier: String! = nil) -> Self! {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier
        
        return tableView.dequeueReusableCell(withIdentifier: reuseIdentifier) as? Self
    }
    
    static func register(to tableView: UITableView, reuseIdentifier: String! = nil) {
        let reuseIdentifier = reuseIdentifier ?? msReuseIdentifier
        
        if let nib = nib() {
            tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        } else {
            tableView.register(self, forCellReuseIdentifier: reuseIdentifier)
        }
        
    }
}
