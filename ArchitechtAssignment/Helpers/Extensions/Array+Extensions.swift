//
//  Array+Extensions.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import Foundation

extension Array where Element == URL {
    mutating func sortByCreateDate() {
        try? sort(by: {
            let date0 = try $0.promisedItemResourceValues(forKeys:[.contentModificationDateKey]).contentModificationDate!
            let date1 = try $1.promisedItemResourceValues(forKeys:[.contentModificationDateKey]).contentModificationDate!
            return date0.compare(date1) == .orderedDescending
        })
    }
}
