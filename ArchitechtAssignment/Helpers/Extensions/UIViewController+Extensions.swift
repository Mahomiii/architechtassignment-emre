//
//  UIViewController+Extensions.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import Foundation
import UIKit

extension UIViewController {
    func showAlert(title: String, message: String, buttonTitle: String = "Ok".localized(), okButtonHandler: (() -> ())? = nil) {
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
            controller.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: { action in
                if let okButtonHandler = okButtonHandler {
                okButtonHandler()
                }
            }))
        
        present(controller, animated: true, completion: nil)
    }
}
