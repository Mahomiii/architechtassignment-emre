//
//  String+Extensions.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
