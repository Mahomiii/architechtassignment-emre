//
//  UIImageView+Extensions.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

extension UIImageView {
    
    public func imageFromURL(urlString: String) {
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
                if error != nil {
                    return
                }
                DispatchQueue.main.async(execute: { () -> Void in
                    if let image = UIImage(data: data!) {
                        self.image = image
                    }
                    
                })
                
            }).resume()
        }
    }
    
    public func loadImageFromFile(fileName: String, size: CGSize = CGSize()) {
        let documentsUrl = LocalFileManager.shared.documentsUrl
        let fileURL = documentsUrl.appendingPathComponent(fileName)
        do {
            let imageData = try Data(contentsOf: fileURL)
            self.image = UIImage(data: imageData)!
        } catch {}
        
    }
}
