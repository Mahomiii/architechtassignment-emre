//
//  Constant.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

let baseUrl = "https://dog.ceo/api/"
let randomImageUrl = baseUrl + "breeds/image/random"

let imagesKey = "images"

let defaultCornerRadius: CGFloat = 8

