//
//  Router.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import UIKit

final class Navigator {
    
    public static let shared = Navigator()
    
    var window: UIWindow?
    
    public func start(_ window: UIWindow?) {
        guard let viewController = HomeBuilder.generate() else { return }
        let navBar = UINavigationController(rootViewController: viewController)
        self.window = window

        guard let window = self.window else {
            print("Window is not configured!!!")
            return
        }
        self.window?.makeKeyAndVisible()
        self.window?.rootViewController = navBar
    }
}

extension Navigator {
    
    func push(_ viewController: UIViewController, from: UIViewController? = nil) {
        DispatchQueue.main.async {
            from?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func pop(_ viewController: UIViewController?) {
        DispatchQueue.main.async {
            viewController?.navigationController?.popViewController(animated: true)
        }
    }
}
