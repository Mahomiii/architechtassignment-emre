//
//  DogResponse.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 18.06.2022.
//

import Foundation

struct DogResponse: Codable {
    let message: String
    let status: String
}

