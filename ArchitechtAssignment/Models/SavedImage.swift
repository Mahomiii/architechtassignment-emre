//
//  SavedImage.swift
//  ArchitechtAssignment
//
//  Created by Emre Temiz on 20.06.2022.
//

import Foundation

struct SavedImage: Codable {
    var fileName: String
    var timestamp: Int64
}
